import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class CraftingPanel extends JPanel implements MouseListener {
	
	private boolean hasImages;
	
	private ItemInfoPanel itemInfoPanel;
	private CraftingInfoPanel craftingInfoPanel;
	
	public CraftingPanel(int width, int height){
		setPreferredSize(new Dimension(width, height));
		itemInfoPanel = new ItemInfoPanel(width, 70);
		craftingInfoPanel = new CraftingInfoPanel(width, (height-150) );
		// leave 80 pixels for the borders
		
		itemInfoPanel.setVisible(true);
		craftingInfoPanel.setVisible(true);
		
		itemInfoPanel.setBorder(BorderFactory.createTitledBorder("Item"));
		craftingInfoPanel.setBorder(BorderFactory.createTitledBorder("Crafting"));
		
		add(itemInfoPanel, BorderLayout.NORTH);
		add(craftingInfoPanel, BorderLayout.SOUTH);
		
		setVisible(true);
		addMouseListener(this);
		hasImages = false;
	}
	
	public void showAllItemsWithTag(CraftingMethod craftingMethod, String tag, ArrayList<Item> items){
		itemInfoPanel.setBorder(BorderFactory.createTitledBorder("Crafting Method"));
		itemInfoPanel.setItemInfoPanel(craftingMethod.getName(), tag);
		craftingInfoPanel.setBorder(BorderFactory.createEmptyBorder());
		craftingInfoPanel.showAllItemsWithTag(tag,items);
		
		revalidate();
		repaint();
	}
	
	public void update(Item item, Recipe recipe, CraftingMethod craftingMethod, int amount){
		itemInfoPanel.setBorder(BorderFactory.createTitledBorder("Item"));
		craftingInfoPanel.setBorder(BorderFactory.createTitledBorder("Crafting"));
		itemInfoPanel.setItemInfoPanel(item);
		if(craftingMethod != null && recipe != null){ //item.isCraftable()
			craftingInfoPanel.setCraftingInfoPanel(item, recipe, craftingMethod, amount);
		} else {
			craftingInfoPanel.informNotCraftable(item);
		}
	}
	
	public void enableImages(boolean b) {
		hasImages = true;
	}
	
	public void findImageWithID(String iD){
		if(hasImages){
			if(iD.contains(":")){
				iD = iD.replace(":", "-");
			} else {
				iD += "-0";
			}
			if(new File("itemicons/" + iD + ".png").isFile()){
				// FIXME: ICON (in craftingPanel.itemInfoPanel perhaps?)
			}
		}
	}
	
	public CraftingInfoPanel getCraftingInfoPanel(){
		return craftingInfoPanel;
	}

	public ItemInfoPanel getItemInfoPanel() {
		return itemInfoPanel;
	}

	

	@Override
	public void mouseClicked(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// Grab focus away from the search bar
		grabFocus();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// Empty: do nothing
	}
	
}
