import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.StringTokenizer;

public class Database extends Observable {

	private ArrayList<Item> items;
	private ArrayList<Recipe> recipes;
	private ArrayList<CraftingMethod> craftingMethods;
	private boolean showingOnlyObtainable;
	
	public Database(){
		items = new ArrayList<>();
		recipes = new ArrayList<>();
		craftingMethods = new ArrayList<>();
		showingOnlyObtainable = false;
	}
	
	public boolean isShowingOnlyObtainable(){
		return showingOnlyObtainable;
	}
	
	public void setShowingOnlyObtainable(boolean value){
		showingOnlyObtainable = value;
	}
	
	public ArrayList<Item> getItems(){
		return items;
	}
	
	public ArrayList<Item> getObtainableItems(){
		ArrayList<Item> obtainableItems = new ArrayList<>();
		for(Item item : items){
			if(item.isObtainable()) obtainableItems.add(item);
		}
		return obtainableItems;
	}
	
	public ArrayList<Recipe> getRecipes(){
		return recipes;
	}
	
	public ArrayList<CraftingMethod> getCraftingMethods(){
		return craftingMethods;
	}
	
	/**
	 * Adders and removers for the recipe, item and crafting method lists
	 */

	public void addItem(Item item){
		items.add(item);
		sortItems();
		setChanged();
		notifyObservers();
	}
	
	public void replaceItem(Item old, Item edited){
		for(Item change : items){
			if(old.equals(change)){
				// Change item values
				old.setName(edited.getName());
				old.setObtainable(edited.isObtainable());
				old.setTags(edited.getTags());
				// Change affected recipes
				if(!old.getID().equals(edited.getID())){
					old.setID(edited.getID());
					for(Recipe recipe : recipes){
						if(recipe.getInput().contains(old)){
							recipe.getInput().set(recipe.getInput().indexOf(old), change);
						}
						if(recipe.getOutput().contains(old)){
							recipe.getOutput().set(recipe.getOutput().indexOf(old), change);
						}
					}
				}
			}
		}
		sortItems();
		setChanged();
		notifyObservers();
	}
	
	public void removeItem(Item item){
		// Remove an item and all recipes that contain this item
		items.remove(item);
		ArrayList<Recipe> deleteList = new ArrayList<>();
		for(Recipe recipe : recipes){
			if(recipe.getInput().contains(item) || recipe.getOutput().contains(item)){
				deleteList.add(recipe);
			}
		}
		for(Recipe recipe : deleteList){
			removeRecipe(recipe);
		}
		sortItems();
		setChanged();
		notifyObservers();
	}
	
	public void addRecipe(Recipe recipe){
		recipes.add(recipe);
		for(Item item : recipe.getOutput()){
			item.addRecipe(recipe);
		}
		setChanged();
		notifyObservers();
	}
	
	public void replaceRecipe(Recipe old, Recipe edited){
		// TODO: check if the following works: old = edited;
		old.setCraftingMethod(edited.getCraftingMethod());
		old.setInput(edited.getInput());
		old.setOutput(edited.getOutput());
		// TODO: check importance of the following for-loop
		for(Item item : items){
			if(item.getRecipes().contains(old)){
				item.getRecipes().set(item.getRecipes().indexOf(old), edited);
			}
		}
		setChanged();
		notifyObservers();
	}
	
	public void removeRecipe(Recipe recipe){
		// Remove all occurrences of the recipe in an output item's recipelist, then remove the recipe itself
		for(Item item : items){
			if(item.getRecipes().contains(recipe)){
				item.removeRecipe(recipe);
			}
		}
		recipes.remove(recipe);
		setChanged();
		notifyObservers();
	}
	
	public void addCraftingMethod(CraftingMethod method){
		craftingMethods.add(method);
		setChanged();
		notifyObservers();
	}
	
	public void replaceCraftingMethod(CraftingMethod old, CraftingMethod edited){
		// Replace recipes containing the crafting method, then the method itself
		// FIXME!
		ArrayList<Recipe> replaceList = new ArrayList<>();
		Recipe replacementRecipe;
		for(Recipe recipe : recipes){
			if(recipe.getCraftingMethod().getName().equals(old.getName())){
				replacementRecipe = recipe;
				replacementRecipe.setCraftingMethod(edited);
				replaceRecipe(recipe, replacementRecipe);
			}
		}
		for(Recipe recipe : replaceList){
			replaceRecipe(recipe, recipe);
		}
		old = edited;
		setChanged();
		notifyObservers();
	}
	
	public void removeCraftingMethod(CraftingMethod method){
		// Remove recipes containing the crafting method, then the method itself
		ArrayList<Recipe> deleteList = new ArrayList<>();
		for(Recipe recipe : recipes){
			if(recipe.getCraftingMethod().getName().equals(method.getName())){
				deleteList.add(recipe);
			}
		}
		for(Recipe recipe : deleteList){
			removeRecipe(recipe);
		}
		craftingMethods.remove(method);
		setChanged();
		notifyObservers();
	}
	
	public void sortItems(){
		Collections.sort(items);
		setChanged();
		notifyObservers();
	}
	
	/**
	 * @param iD the ID of an item to be found
	 * @return the item with the given ID
	 */
	
	public Item findItemByID(String iD){
		for(Item item : items){
			if(iD.equals(item.getID())) return item;
		}
		// No item exists with the given ID
		return null;
	}
	
	public CraftingMethod findCraftingMethodByName(String name){
		for(CraftingMethod method : craftingMethods){
			if(name.equals(method.getName())) return method;
		}
		// No crafting method exists with the given name
		return null;
	}
	
	/**
	 * Methods 
	 * @return the ID next in line
	 */
	
	public String getNextAvailableID(){
		int tempID = 0;
		for(Item item : items) {
			// Make sure to loop only over items without metadata (data with a ":")
			if(!item.getID().contains(":")){
				if(tempID == Integer.parseInt(item.getID())) {
					tempID += 1;
				} else {
					return Integer.toString(tempID);
				}
			}
		}
		return Integer.toString(tempID);
	}
	
	/**
	 * Clear all data 
	 */
	
	public void clearData(){
		items.clear();
		recipes.clear();
		craftingMethods.clear();
		setChanged();
		notifyObservers("Data cleared!");
	}
	
	/**
	 * Loads a file containing items and recipes
	 * @param path is the location of the file to be loaded
	 */
	
	public void loadFile(String path){
		StringTokenizer token = null;
		BufferedReader input = null;
		
		try {
			input = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			System.err.println("FILE NOT FOUND! Please check if you supplied the right file!");
			e.printStackTrace();
			return;
		}
		
		int numItems = 0, numRecipes = 0, numMethods = 0;
		
		// Item declarations
		String iD, name, line = null;
		boolean isObtainable;
		ArrayList<String> tags = new ArrayList<>();
		
		// CraftingMethod declarations
		String craftingName;
		String description;
		ArrayList<String> tag = new ArrayList<>();
		int inputWidth, inputHeight, outputWidth, outputHeight;
		
		// Recipe declarations
		String recipeCraftingMethod;
		ArrayList<Item> recipeInput = new ArrayList<>();
		ArrayList<Item> recipeOutput = new ArrayList<>();
		
		try {
			// Reading the items
			input.readLine(); // Skip first 'comment' line
			line = input.readLine();
			token = new StringTokenizer(line);
			while(!line.startsWith("<crafting method definition>")){
				token = new StringTokenizer(line);
				iD = token.nextToken("\t");
				name = token.nextToken();
				isObtainable = Boolean.parseBoolean(token.nextToken("\t"));
				while(token.hasMoreTokens()){
					tags.add(token.nextToken());
				}
				items.add(new Item(iD, name, isObtainable, tags));
				tags = new ArrayList<>();
				numItems++;
				line = input.readLine();
			}
			notifyObservers("Number of items loaded: " + numItems);
			
			// Reading the crafting methods
			line = input.readLine();
			while(line != null && !line.startsWith("<")){
				token = new StringTokenizer(line);
				craftingName = token.nextToken("\t");
				inputWidth = Integer.parseInt(token.nextToken());
				inputHeight = Integer.parseInt(token.nextToken());
				outputWidth = Integer.parseInt(token.nextToken());
				outputHeight = Integer.parseInt(token.nextToken());
				description = token.nextToken();
				while(token.hasMoreTokens()){
					tag.add(token.nextToken());
				}
				craftingMethods.add(new CraftingMethod(craftingName, inputWidth, inputHeight, outputWidth, outputHeight, description, tag));
				numMethods++;
				line = input.readLine();
			}
			notifyObservers("Number of crafting methods loaded: " + numMethods);
			
			// Reading the recipes
			line = input.readLine();
			while(line != null){
				token = new StringTokenizer(line);
				recipeCraftingMethod = token.nextToken("\t");
				String testToken = token.nextToken();
				// Skip "<input:>" token
				testToken = token.nextToken();
				while(!testToken.equals("<output:>")){
					recipeInput.add(findItemByID(testToken));
					testToken = token.nextToken();
				}
				while(token.hasMoreTokens()){
					recipeOutput.add(findItemByID(token.nextToken()));
				}
				recipes.add(new Recipe(findCraftingMethodByName(recipeCraftingMethod), recipeInput, recipeOutput));
				recipeInput = new ArrayList<>();
				recipeOutput = new ArrayList<>();
				numRecipes++;
				line = input.readLine();
			}
			notifyObservers("Number of recipes loaded: " + numRecipes);
			

			
		} catch (NoSuchElementException e) {
			System.err.println("NO MORE TOKENS! Tried to add nonexistent tokens to list from the following line:\n\"" + line + "\"\nPlease check input file.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Something went wrong probably with \"input.readLine()\". See stacktrace for more details:");
			e.printStackTrace();
			System.exit(-3);
		}
		
		// Every item has an arraylist of recipes which have the item as the output - these arrays will now be filled with the correct recipes for quick retrieval
		Item item;
		for(Recipe recipe : recipes){
			for(Item output : recipe.getOutput()){
				item = findItemByID(output.getID());
				if(item != null){
					if(!item.getRecipes().contains(recipe)){
						item.addRecipe(recipe);
					}
				} else {
					System.err.println("Recipe " + recipes.indexOf(recipe)+1 + " expects non-existent item with ID " + output.getID() + "!");
				}
			}
		}
		setChanged();
		notifyObservers("File loaded!");
	}
	
	/**
	 * Saves a file containing items and recipes
	 * @param path is the location of the file to be saved
	 */
	public void saveFile(String path){
		BufferedWriter output = null;
		
		// Fix file extension if necessary, so that every file is saved neatly as .tsv (Tab-Separated Values)
		if(!path.endsWith(".tsv")){
			path += ".tsv";
		}
		
		try {
			output = new BufferedWriter(new FileWriter(path));
		} catch (IOException e) {
			System.err.println("Something went wrong. Perhaps there already is a file '" + path + "'. See stacktrace for more details:");
			e.printStackTrace();
		}
		
		try {
			
			// Info line about items			
			output.write("<ID>\t<name>\t<isObtainable>\t<tags>");
			
			for(Item item : items) {
				output.newLine();
				output.write(item.toString());
			}
			
			// Info line about crafting methods
			output.newLine();
			output.write("<crafting method definition>");
			
			for(CraftingMethod method : craftingMethods){
				output.newLine();
				output.write(method.toString());
			}
						
			// Info line about recipes
			output.newLine();
			output.write("<crafting method>\t<input item ID>\t<output item IDs>");
			
			for(Recipe recipe : recipes) {
				output.newLine();
				output.write(recipe.toString());
			}
			
			output.close();
			
		} catch(IOException e){
			System.err.println("Something went wrong with the output file. See stacktrace for more details:"); 
			e.printStackTrace();
		}
		notifyObservers("File saved!");
	}
	
}
