import java.util.ArrayList;

public class CraftingMethod {
	
	private String name;
	private int inputWidth, inputHeight, outputWidth, outputHeight; //for storing the input and output recipe sizes.
	private String description; // Description of a crafting method in the application for the user to read and understand this method
	private ArrayList<String> tags; //for stuff like smeltable
	
	public CraftingMethod(String name, int inputWidth, int inputHeight, int outputWidth, int outputHeight){
		this(name, inputWidth, inputHeight, outputWidth, outputHeight, null, null);
	}

	public CraftingMethod(String name, int inputWidth, int inputHeight, int outputWidth, int outputHeight, String description, ArrayList<String> tags){
		this.name = name;
		this.inputWidth = inputWidth;
		this.inputHeight = inputHeight;
		this.outputWidth = outputWidth;
		this.outputHeight = outputHeight;
		this.description = (description == null || description == "" ? "No description yet." : description);
		this.tags = (tags == null || tags.isEmpty() ? null : tags);
	}
	
	public String toString(){
		String returnString = name;
		returnString += "\t" + inputWidth + "\t" + inputHeight + "\t" + outputWidth + "\t" + outputHeight + "\t" + description;
		if(tags != null && !tags.isEmpty()){
			for(String tag : tags){
				returnString += "\t" + tag;
			}
		}
		return returnString;
	}

	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}
	
	public ArrayList<String> getTags(){
		return tags;
	}
	
	public boolean hasTags(){
		return (tags == null ? false : !tags.isEmpty() );
	}

	public int getInputWidth() {
		return inputWidth;
	}

	public int getInputHeight() {
		return inputHeight;
	}

	public int getOutputWidth() {
		return outputWidth;
	}

	public int getOutputHeight() {
		return outputHeight;
	}
	
	public int getInputSize() {
		return inputWidth * inputHeight;
	}
	
	public int getOutputSize() {
		return outputWidth * outputHeight;
	}

}
