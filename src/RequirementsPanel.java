import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class RequirementsPanel extends JPanel {
	
	private ArrayList<JLabel> text;
	private JSpinner craftAmount;
	private Item item;
	private Recipe recipe;
	private AppFrame listener;
	private JPanel container;
	private JButton button;
	
	public RequirementsPanel(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		text = new ArrayList<>();
		setText("No item has been selected.");
		setVisible(true);
	}

	public void setText(String s){
		clearText();
		text.add(new JLabel(s));
		add(text.get(0));
	}
	
	public void clearText(){
		for(JLabel jlabel : text){
			remove(jlabel);
		}
		text = new ArrayList<>();
	}
	
	public void setRequirementsPanel(Item item, Recipe recipe, int amount){		
		this.item = item;
		this.recipe = recipe;
		
		clearText();
		if(container != null){
			remove(container);
		}
		container = new JPanel();
	    container.setBorder(BorderFactory.createLineBorder(Color.black));
	    BoxLayout layout = new BoxLayout(container, BoxLayout.Y_AXIS);
	    container.setLayout(layout);
		
	    if(craftAmount != null) {
			remove(craftAmount);
		}
		SpinnerModel model = new SpinnerNumberModel(amount, 0, 10000, 1); 
		craftAmount = new JSpinner(model);
		craftAmount.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent arg0) {
				onSelectionChange();
			}		
		});
		container.add(craftAmount);
		
		ArrayList<String> input = calculateRequirements(item, recipe, amount);
		StringTokenizer token = null;
		String name = "", id = "";
		
		for(String line : input) {
			token = new StringTokenizer(line, "\t");
			amount = Integer.parseInt(token.nextToken());
			name = token.nextToken();
			if(token.hasMoreTokens()) {
				id = token.nextToken();
			}
			
			button = new JButton(stack(amount) + " : " + name + (id == null ? "" : " (ID " + id + ")"));
			button.setAlignmentX(Component.CENTER_ALIGNMENT);
			button.addMouseListener(new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					onButtonPress((JButton) arg0.getSource());
				}
				@Override
				public void mouseEntered(MouseEvent e)	{ /* unused */ }
				@Override
				public void mouseExited(MouseEvent e)	{ /* unused */ }
				@Override
				public void mousePressed(MouseEvent e)	{ /* unused */ }
				@Override
				public void mouseReleased(MouseEvent e)	{ /* unused */ }
			});			
			container.add(button);
			id = null;
		}
		add(container);
		
		revalidate();
		repaint();
	}
	
	public void onButtonPress(JButton buttonx){
		String name = buttonx.getText();
		if(name.contains("ID ")){
			name = name.split("ID ")[1];
			name = name.substring(0, name.length()-1);
			notifyListeners(name); // item
		} else {
			name = name.split(" : ")[1];
			notifyListeners(recipe.getCraftingMethod(), name); // tag
		}
	}

	public void informNotCraftable(){
		if(container != null){
			remove(container);
		}
		setText("<html>There is no available crafting recipe for this item.</html>");
	}
	
	protected void onSelectionChange() {
		// Auto-generated method stub
		setRequirementsPanel(this.item, this.recipe, (int) craftAmount.getValue());
	}
	
	public String stack(int amount){
		String returnText = "";
		if(amount >= 64){
			returnText += amount/64 + "*64";
			if(amount%64 != 0){
				returnText += " + " + amount%64;
			}
		} else {
			returnText = amount + "";
		}
		return returnText;
	}

	public ArrayList<String> calculateRequirements(Item item, Recipe recipe, int amount){
		// Calculate the number of input items required for a number of resulting items
		ArrayList<String> formatList = new ArrayList<String>();
		ArrayList<Item> inputItems = new ArrayList<>();
		int[] inputAmounts = new int[recipe.getInput().size()];
		Item current = null;
		Iterator<Item> inputIterator = recipe.getInput().iterator();
			
		amount = (int) Math.ceil((double) amount / recipe.getAmount(item)); 
		// we now know how many times we have to do the recipe		
						
		while(inputIterator.hasNext()) {
			current = inputIterator.next();
			if(inputItems.contains(current)){
				inputAmounts[inputItems.indexOf(current)] += 1;
			} else {
				inputAmounts[inputItems.size()] = 1;
				inputItems.add(current);
			}
		} // get all items from this recipe and count them.
		
		for(Item inputItem : inputItems){
			if(!inputItem.getID().equals("0")) {
				formatList.add(inputAmounts[inputItems.indexOf(inputItem)]*amount + "\t" + inputItem.getName() + "\t" + inputItem.getID() );
				// Turn recipe INPUT into counted item input strings (format: amount "tab" itemName "tab" itemID), 
				//	where "tab" ("\t") wil be the Token delimiter.
			}
		}
		
		if(recipe.getCraftingMethod().hasTags()){
			for(String tag : recipe.getCraftingMethod().getTags()) {
				formatList.add(amount + "\t" + tag);
			}
		}
		
		return formatList;
	}
	
	public void addListener(AppFrame listener) {
		this.listener = listener;
	}
	
	private void notifyListeners(String s){
		listener.onNotify(s);
	}
	
	private void notifyListeners(CraftingMethod craftingMethod, String name) {
		listener.onNotify(craftingMethod, name);
		
	}
	

}
