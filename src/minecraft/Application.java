import java.io.File;

/** @author Kevin Nauta & Herman Groenbroek */

public class Application {
		
	/**
	 * The main function. This will construct the AppFrame, and in turn all other panels, and if the default file is present, load that file.
	 * @param arg Any arguments passed onto the main function, they are not used.
	 */
	public static void main(String[] arg) {
		String defaultFilePath = "../minecraft/minecraftrecipes.tsv";
		
		AppFrame frame = new AppFrame();
		
		if(new File(defaultFilePath).isFile()){
			frame.setFilePath(defaultFilePath);
			frame.getData().loadFile(defaultFilePath);
			frame.menuToggleObtainable();
		} else {
			frame.menuNew();
		}
		if(new File("itemicons/").isDirectory()){
			frame.enableImages();
		}
		frame.clearText();
	}

}
