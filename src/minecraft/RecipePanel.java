import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class RecipePanel extends JPanel {
		
	private Item item;
	private Recipe recipe;
	private CraftingMethod craftingMethod;
	private ArrayList<JLabel> text;
	private AppFrame listener;
	private JComboBox<String> recipeList;
	
	/**
	 * This is the constructor for the RecipePanel.
	 * This panel shows the recipe for a specified item, and if the item has multiple items, it will show a dropdown list to select a certain recipe.
	 * @param width The preferred width of the panel.
	 * @param height The preferred height of the panel.
	 */
	public RecipePanel(int width, int height) {
		setPreferredSize(new Dimension(width, height));
		text = new ArrayList<>();
		setText("<html>Recipe Calculator Program<br>By Herman Groenbroek & Kevin Nauta<br>(C) 2015</html>");
		
		setVisible(true);
	}

	public void onSelectionChange(){
		if(recipeList.getSelectedIndex() >= 0 && recipeList.getSelectedIndex() < item.getRecipes().size()){
			notifyListeners(item, recipeList.getSelectedIndex());
		}
	}
	
	/**
	 * When an item is clicked in the ItemPanel or a recipe is selected from this panel, this method is called to show the selected item and recipe.
	 * @param item The item to be displayed.
	 * @param recipe The selected recipe to be displayed.
	 * @param craftingMethod The recipes' crafting method.
	 * @see ItemPanel
	 * @see Item
	 * @see Recipe
	 */
	public void setRecipePanel(Item item, Recipe recipe, CraftingMethod craftingMethod) {
		this.item =item;
		this.recipe = recipe;
		this.craftingMethod = craftingMethod;
		String text = null;
		
		if(recipeList != null){
			remove(recipeList);
		}
		
		if(item.getRecipes().size() > 1) {	
			recipeList = new JComboBox<>();
			int i = 0;
			for(Recipe recipes : item.getRecipes()) {
				recipeList.addItem("<html>" + (1+i++) + ". " + recipes.getSummary() + "</html>");
			}
			recipeList.setSelectedIndex(item.getRecipes().indexOf(recipe));
			recipeList.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					System.out.println(recipeList.getSelectedIndex());
					onSelectionChange();
				}			
			});
			add(recipeList);
		}
				
		int count = 0;
		try{
			text = "<html><center>" + craftingMethod.getName() + "<br><table border=1 style='width:100%'>";
			for(int i = 0; i < craftingMethod.getInputHeight(); i++) {
				text += "<tr>";
				for(int j = 0; j < craftingMethod.getInputWidth(); j++) {
					text += "<td style='text-align:center;width:75px;height:40px'>";
					Item input = recipe.getInput().get(craftingMethod.getInputHeight()*i + j);
					if(! input.getID().equals("0") ) {
						text += input.getName();
					}
					text += "</td>";
					count++;
				}
				text += "</tr>";
			}
		} catch( IndexOutOfBoundsException e) {
			System.err.println("SegFault!");
			System.err.println(text.substring(6)); // DEBUG
			e.printStackTrace();
		}
		
		text += "</table><br> <center>&#8594</center> <br>";
		
		count = 0;
		try{
			text += "<table border=1 style='width:100%'>";
			for(int i = 0; i < craftingMethod.getOutputHeight(); i++) {
				text += "<tr>";
				for(int j = 0; j < craftingMethod.getOutputWidth(); j++) {
					text += "<td style='text-align:center;width:75px;height:40px'>";
					Item output = recipe.getOutput().get(craftingMethod.getOutputHeight()*i + j);
					if(! output.getID().equals("0") ) {
						text += recipe.getAmount(output) + " * " + output.getName();
					}
					text += "</td>";
					count++;
				}
				text += "</tr>";
			}
		} catch( IndexOutOfBoundsException e) {
			System.err.println("SegFault!");
			System.err.println(text.substring(6)); // DEBUG
			System.err.println(recipe);
			e.printStackTrace();
		}
		text += "</table></center></html>";
		setText(text);
	}
	
	/**
	 * If an item is selected in the InfoPanel, that does not have any recipes, this method will show a message that no recipes have (yet) been added to the item.
	 * @param item The selected item, that thus far does not have any recipes.
	 */
	public void informNotCraftable(Item item){
		if(recipeList != null) {
			remove(recipeList);
		}
		
		String text = "<html>The item is not craftable";
		if(item.isObtainable()){
			text += "<br>However, it is obtainable in Survival mode";
		} else {
			text += " nor obtainable in Survival mode";
		}
		text += "<br><br>We suggest you search the Minecraft Wiki for \"" + item.getName() + "\"</html>";
//		text += "<a href='http://minecraft.gamepedia.com/" + item.getName().replaceAll(" ", "_").toLowerCase() + "'>";

		setText(text);
	}
	
	public void setText(String s){
		clearText();
		text.add(new JLabel(s));
		add(text.get(0));
	}
	public void clearText(){
		for(JLabel jlabel : text){
			remove(jlabel);
		}
		text = new ArrayList<>();
	}
	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public CraftingMethod getCraftingMethod() {
		return craftingMethod;
	}

	public void setCraftingMethod(CraftingMethod craftingMethod) {
		this.craftingMethod = craftingMethod;
	}
	
	public void addListener(AppFrame listener) {
		this.listener = listener;
	}
	
	private void notifyListeners(Item item, int index){
		listener.onNotify(item, index);
	}
}
