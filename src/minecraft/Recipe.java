import java.util.ArrayList;

public class Recipe {
	
	private CraftingMethod craftingMethod; // Crafting method (i.e. crafting table, furnace, etc.)
	private ArrayList<Item> output; // List of items resulting from the recipe
	private ArrayList<Item> input; // List of items required for crafting
	
	/**
	 * This is the constructor for a Recipe.
	 * It consists of a method that is used as template for the recipe, as well as a  list of input and output items.
	 * @param method The crafting method used to create this recipe.
	 * @param input The list of items required to make the output.
	 * @param output The list of items created by the input.
	 * @see Item
	 * @see CraftingMethod
	 * @see RecipePanel
	 */
	public Recipe(CraftingMethod method, ArrayList<Item> input, ArrayList<Item> output){
		this.craftingMethod = method;
		this.input = input;
		this.output = output;
	}

	public CraftingMethod getCraftingMethod() {
		return craftingMethod;
	}
	
	public void setCraftingMethod(CraftingMethod craftingMethod) {
		this.craftingMethod = craftingMethod;
	}
	
	public ArrayList<Item> getOutput(){
		return output;
	}
	
	public void setOutput(ArrayList<Item> output) {
		this.output = output;
	}
	
	public ArrayList<Item> getInput() {
		return input;
	}
	
	public void setInput(ArrayList<Item> input) {
		this.input = input;
	}
	
	/**
	 * While the input is saved as being the list of items, the output is saved as being an ordered list of items that may occur multiple times but still should only be printed once.
	 * This method is to return how many times the specified item is created with the recipe.
	 * @param item The item of which the amount is required
	 * @return The amount of times the item is in the list of output items, and as such, the amount of this item is made from one recipe.
	 */
	public int getAmount(Item item){
		int count = 0;
		for(Item x : output){
			if(x.getID().equals(item.getID())){
				count += 1;
			}
		}		
		return count;
	}
	
	public ArrayList<Item> getOutputItems(){
		ArrayList<Item> returnList = new ArrayList<>();
		for(Item item : output){
			if( (! returnList.contains(item)) || item.getID().equals("0")){
				returnList.add(item);
			}
		}
		
		return returnList;
	public void removeFromInput(Item item){
		input.remove(item);
	}
	
	public void addToOutput(Item item){
		output.add(item);
	}
	
	public void removeFromOutput(String iD){
		output.remove(iD);
	}
	
	/**
	 * This method will return a summary of the recipe, containing the name of the crafting method, and all unique items in the input and output item list.
	 * To make sure the summary doesn't grow too long, an enter is printed if the string is longer than 50 characters before printing an item.
	 * This summary is used in selecting a recipe in the RecipePanel, and selecting a recipe to edit.
	 * @return A string containing a summary of the recipe.
	 */
	public String getSummary(){
		ArrayList<Item> summary = new ArrayList<>();
		String text = "[" + craftingMethod.getName() + "] ";
		
		for(Item item : input) {
			if(! summary.contains(item)){
				summary.add(item);
			}
		}
		int length = 50;
		for(Item item : summary){
			if(!item.getID().equals("0")){
				text += item.getName() + " + ";
				if(text.length() > length){
					text += "<br>";
					length += 54;
				}
			}
		}
		text = text.substring(0, text.length()-2) + " &#8594 ";
		length += 1;

		summary = new ArrayList<>();
		for(Item item : output) {
			if(! summary.contains(item)){
				summary.add(item);
			}
		}
		for(Item item : summary){
			if(!item.getID().equals("0")){
				if(text.length() > length){
					text += "<br>";
					length += 54;
				}
				text += item.getName() + " + ";
			}
		}
			
		return text.substring(0, text.length()-2);
	}
	
	public String toString(){
        String returnString = craftingMethod.getName() + "\t<input:>";
        for(Item item : input){
        	returnString += "\t" + item.getID();
        }
        returnString += "\t<output:>";
        for(Item item : output){
        	returnString += "\t" + item.getID();
        }
        return returnString;
	}
	
}
